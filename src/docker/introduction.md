# Docker tutorial 
***by `raatty#3522`***

---

## What is it?

It is a lightweight containerized system, it is somewhere between a VM and running your code on your computer. The reason I find it good is mainly because it is all scripted and therefore reproducible.
So it makes it easy for someone else to get the exact same environment as you if you have a problem or some because normally there are all sorts of ways you could have installed Python or whatever that could be causing the problem.
This tutorial will cover what you need to know to run your Discord bot on Docker.
