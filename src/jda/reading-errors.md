# Fixing and Detecting errors

Everyone loves to fix something by themselves once! At least I do.

So, when you get an error, how do you read and act based on it? Well, it's simple. You read the error first, very carefully, and read to the region it specifies very carefully as well. (There might be some typos causing it), second, try using a debugger. It tells you everything and it's self explanatory, and it also detects faulty code and suggests code for you on the fly sometimes. Those will minimize your chance of error. And now into how to act upon it.

Everyone's first error might be different, sometimes you won't even get an error, it just wont work. ***`(That's probably because you forgot a .queue() somewhere.)`***

So, here comes an error:

```
[ERROR] /jdatut/src/main/java/Main.java:[30,25] unreported exception javax.security.auth.login.LoginException; must be caught or declared to be thrown
[ERROR] /jdatut/src/main/java/Main.java:[31,31] unreported exception java.lang.InterruptedException; must be caught or declared to be thrown
[INFO] 2 errors
```

Hmmm, so here, it's mentioning the exact file that is going wrong, It's mentioning 2 lines, and one of them is empty!

```java
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import javax.security.auth.login.LoginException;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.Activity;

public class Main {
	public static void main(String[] args) {
	    File tokenFile = new File("/storage/emulated/0/jdatut/token.txt");
	    String token = null;
	    try(Scanner s = new Scanner(tokenFile)) {
	        if(s.hasNextLine()) {
	            token = s.nextLine();
	        } else {
	            throw new RuntimeException("The token file is empty");
	        }
	    } catch(IOException e) {
	        System.out.println("File not found.");
	    }
	    // line 25
		JDA jda = new JDABuilder(token)
		  .setActivity(Activity.playing("with Finn & Jake"))
		  .setStatus(OnlineStatus.DO_NOT_DISTURB)
		  .addEventListeners(new PingCmd())
		  .build(); // line 30
		jda.awaitReady();
		System.out.println("IM READY!!! prefix is jt!");
	}
}
```

So at what might it be marking?

It's pointing at the places where the errors would happen in, not where you should fix it, but most of the time those are valid places to fix it, let's read further into it!

`unreported exception javax.security.auth.login.LoginException; must be caught or declared to be thrown` Hmmm... Must be caught or declared to be thrown. Doesn't that mean its an exception that has a chance to happen, so we should catch, but what does throw mean?

`[31,31] unreported exception java.lang.InterruptedException; must be caught or declared to be thrown` That's the same thing!

How about we google `throwing an exception in java`?

if we do, we end up at www.geeksforgeeks.org/throw-throws-java which tells us all about throwing and everything!

You should always be googling your errors or some parts of it to understand it more

According to geeksforgeeks, we should be using the `throws` keyword in the main method to throw the `LoginException` and `InterruptedException`, which we should import from `javax.security.auth.login` and `java.lang` respectively (which we can easily do using the debugger)

```java
public class Main {
	public static void main(String[] args) throws LoginException, InterruptedException {
```
Tadaa! Now its all good! make sure to remember to ask us in the #jda-help channel if something goes wrong!