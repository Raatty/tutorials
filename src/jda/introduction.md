# JDA tutorial
***`by M.A#4999`***

---
Welcome! this is a tutorial for making bots using JDA!

But first, you **DO** need knowledge on how to use Java, I'm not going to explain the very basics, but for learning Java, you could go to [Codeacademy's Java tutorial](https://www.codecademy.com/learn/learn-java) or [Learn java online](https://www.learnjavaonline.org)

Also, you must download the JDK, found in [Oracle's site](http://www.oracle.com/technetwork/java/javase/downloads/index.html) click on JDK, and install the one appropriate for your OS (if you haven't already) 

Or if you have linux, you can open the terminal, (You can with ctrl+alt+t) then type `sudo apt install openjdk-12` (you can change 12 to whatever version you want, but I'll be defaulting into this one.)
You can also download it from [openjdk](https://openjdk.java.net/) too!


**Introduction to JDA:**

JDA is a discord library for java, made for you to be able to construct, and make your very own discord bot! this tutorial will explain how could you use it.

Also, you can always check the GitHub for jda found in [their github page](https://github.com/DV8FromTheWorld/JDA) and [check the wiki](https://github.com/DV8FromTheWorld/JDA/wiki), and [look at the docs](https://ci.dv8tion.net/job/JDA/javadoc/)

also, this tutorial assumed that you have a bot account that is in 1 guild or more and it's token copied with you.
 

if you want to make one, then head over to [Making the bot](https://dcacademy.gitlab.io/tutorials/starting/making-the-bot.html)

this tutorial uses [IntelliJ IDEA](https://www.jetbrains.com/toolbox/app/) download the toolbox then download IntelliJ mode from there (so you can update intelliJ easily), and this tutorial also uses gradle/maven. You can follow this tutorial with any IDE or text editor but with different stuff for some things.

![Install IntelliJ From Toolbox](./imgs/instalation_from_the_toolbox.gif)

Let's move on to [Getting Started](./getting-started.html)