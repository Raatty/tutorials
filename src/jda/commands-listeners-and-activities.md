# Commands, Listeners and Activities

Now we're going to make the commands for our bot! 

I'm just going to make a simple ping-pong command, you can see the docs here if you want insight on what you'd want to do, I highly recommend you do read them: https://ci.dv8tion.net/job/JDA/javadoc/

For making other commands, you can go to the docs like it says above! and ask us in the `jvm-help` channel on our discord server if you've got any questions!

Now, we're gonna make a new class that extends ListenerAdapter!

For that, I'm going to add a class to the src folder (or create a new folder and call it commands and add it in there, you also can create folders and name them based on the catgeory of the command you want to add, e.g. tictactoe belongs in fun) and name the class PingCommand (the listeneradapters behave as events and are used to make "commands", you can make your own command framework)

```java
public class PingCommand extends ListenerAdapter {
  // code
}
``` 
Also, this is very simple if you have some experience about java!

Now, in the class, we're going to override the onMessageRecieved method! this is an event that triggers when a message is sent in a guild that the bot is in! and we use it like this:

```java
@Override
public void onGuildMessageRecieved(MessageRecievedEvent event)
{
  if(event.getAuthor().isBot()) return;
 // this statement checks if the Author of the message sent is a bot, and if the author of the message is a bot, it doesnt respond.

  Message msg = event.getMessage();
  // gets the message that triggered the event

  if(msg.getContentRaw().equals("!ping")) 
  // msg.getContentRaw() gets the message's
  // content with the markdown and hidden
  // characters, even the \ if it happened to
  // be after a symbol, in which discord makes
  // it disappear.
  {
    MessageChannel channel = event.getChannel();
    channel.sendMessage("Pong!').queue();
    // Important to call .queue()
  }
}
``` 
Here, you could realize that reading the comments would be very useful! Now, this code would not work with the bot if we run it, thats because we havent added it as a listener for the bot! Now, do you remember the JDA builder we used to make our bot come online? lets go back to it!
```java
JDA jda = new JDABuilder(token).build();

jda.awaitReady();
```

Now, in this code, we add a simple line to register our listener! the line is `.addEventListener(new PingCommand())` and add it right before the `.build()` line! 

> NOTICE: you can replace `PingCommand` in the `.addEventListener(new PingCommand())` line to your class's (that extends ListenerAdapter) name followed by ()

Now, the JDABuilder code is:
```java
JDA jda = JDABuilder(token)
          .addEventListener(new PingCommand())
          .build();

jda.awaitReady()
```
You can add as much listeners as you want!

Which would lead us to the full code:

```java
public class Main
{
  public static void main(String[] args) throws LoginException
  {
    JDA jda = new JDABuilder(token)
          .addEventListener(new PingCommand())
           .build();
    jda.awaitReady();
  }
}

// in another class called PingCommand
public class PingCommand extends ListenerAdapter
{
  @Override
public void onMessageRecieved(MessageRecievedEvent event)
{
  if(event.getAuthor().isBot()) 
    return;
   // this statement checks if the Author of the message sent is a bot, and if the author of the message is a bot, it doesnt respond.
  if(event.isFromType(ChannelType.PRIVATE)) return;
  // this if statement checks if the message 
  // is in the DMs, and ignores it if it is, 
  // if you dont want that to happen, 
  // delete or modify this line to your needs

  Message message = event.getMessage();
  // gets the message that triggered the event
  if(msg.getContentRaw().equals("!ping"))
  // msg.getContentRaw() gets the message's
  // content with the markdown and hidden
  // characters, even the \ if it happened to
  // be after a symbol, in which discord makes
  // it disappear.
  {
    MessageChannel channel = event.getChannel();
    channel.sendMessage("Pong!").queue();
    // Important to call .queue()
  }
 }
}
``` 

So..... You know the cool bots, that have custom amazing playing status?
I'm going to teach you all about it today!

Now, the playing status thing doesnt need a genius to make it, and its quite simple! But we need to get to our JDABuilder lines Again! those are pretty important if you couldnt already tell!
```java 
JDA jda = new JDABuilder(token)
          .addEventListener(new PingCommand())
          .build();
jda.awaitReady();
``` 
Now, we need to add another line above the `.build()` line again! 
see? this is getting simple and easy!

Now, here's the line to make a playing status:
`.setActivity(Activity.playing("gameNameHere"))` this'd show:
**playing** gameNameHere

Now, you can change the gameNameHere to make it show anything, like this:
`.setActivity(Activity.playing("With Finn & Jake"))` this'd display
**Playing** with Finn & Jake 

as your bot's status! Now, this is what the JDABuilder lines would look like:

```java
JDA jda = new JDABuilder(token)
          .addEventListener(new PingCommand())
          .setActivity(Activity.playing("with Finn & Jake"))
          .build();

jda.awaitReady();
```

Now, on to watching and Listening to and Streaming statuses!

You could switch `Activity.playing` with any status you want! Like
`Activity.listening("you")` 
this will show

**Listening to** you

(psst, you can alsp use the `setStatus` function to set the status of your bot!)

e.g.
```java
JDA jda = new JDABuilder(token)
          .addEventListener(new PingCommand())
          .setActivity(Activity.playing("with Finn & Jake"))
          .setStatus(OnlineStatus.DO_NOT_DISTURB)
          .build();

jda.awaitReady();
```
And since I never explained what `jda.awaitReady();` does, its blocking the bot and that guarantees that JDA will be completely loaded before working. Meaning that every line after it, e.g. If I add a println there after that line

`System.out.println("JDA successfully loaded!");`

It'd print that after JDA loads, because it's blocking.

(Meaning that it's not only limited to prints, you can do whatever after that line after JDA loads.)

Whenever you're ready, go into [Embeds](./embeds.html)