# ReadyEvent and Logging

Now let's learn about how we can use everything to our advantage.

JDA documents most important stuff on the [docs](https://ci.dv8tion.net/job/JDA/javadoc/), so it will help you alot with your journey on making a bot and its commands, plus some neat events.

(Note to always put `@Override` before the function declaration for the function to work)
The reason we put `@Override` before the line of the function of the command is because the event is already declared, so you need to override that declaration with yours that has your code for the behaviour to change.


What we're gonna learn on this is MORE useful events! (other than the onMessageRecieved event) isnt that just exciting?

Now, we're gonna learn about the onReady event; that's when the bot's ready to recieve and send events

We do the same as we did when we wanted to add an event, make a new class and name it whatever you want, I'll be naming it "BotReadyEvent.java"

then we use almost the same steps with the message listener.

we make it extend ListenerAdapter, then override the OnReady event with the argument event of the type `ReadyEvent`

```java
public class BotReadyEvent extends ListenerAdapter {
    public void onReady(ReadyEvent event) {
        System.out.println("Im ready, Im ready, Im ready!");
    }
}
```

This would be the BotReadyEvent class's code, now its to putting it on our JDA in our main class.

we go back to this portion

```java
JDA jda = new JDABuilder(token)
          .addEventListener(new PingCmd())
          .setActivity(Activity.playing("with Finn & Jake"))
          .setStatus(OnlineStatus.DO_NOT_DISTURB)
          .build();

jda.awaitReady();
```

and just after the line where we defined JDA and before the line where we added the PingCmd ListenerAdapter in, we add the BotReadyEvent for the ReadyEvent to work, like this:

`.addEventListener(new BotReadyEvent())`

If you named your class any other name, you should change `BotReadyEvent` with it.

Of course, the `ReadyEvent event` argument we added has some properties!

you can use it to get the total number of guilds after the bot is ready, e.g.
(do this after you defined `ReadyEvent event` in your ready event)
`event.getGuildTotalCount();` would get the count of all available and unavailable guilds, to know how to get only the available or unavailable guilds i suggest you visit the docs at https://ci.dv8tion.net/job/JDA/javadoc/

# Setting up logback

Alright, so we're gonna set up a logger in our bot, a logger is better than normal is because it doesnt take as much effort as printlns, and it logs any error (or everything, if you've set it to. Or left it on default), JDA needs a slf4j logger (slf4j or ones based on it), and logback is based on slf4j, and its one of the easiest to set up, you just need to put it in your pom.xml or build.gradle, then you have 1 step after: make a logback.xml and put it anywhere in your classpath, then add the configuration

All of which can be found in the [logback site that has docs, faqs, code, etc](https://logback.qos.ch/index.html)

In the first step, you'd have to put in information about how you'd want your logging messages are formatted in logback.xml, here's the default thing in xml:
```xml
<configuration>

  <appender name="STDOUT" class="ch.qos.logback.core.ConsoleAppender">
    <!-- encoders are assigned the type
         ch.qos.logback.classic.encoder.PatternLayoutEncoder by default -->
    <encoder>
      <pattern>%d{HH:mm:ss.SSS} [%thread] %-5level %logger{36} - %msg%n</pattern>
    </encoder>
  </appender>

  <root level="debug">
    <appender-ref ref="STDOUT" />
  </root>
</configuration>
```
You can change it however you want to make one that satisfies you, e.g.maybe remove the seconds, or remove the SSS so it doesn't show microseconds. Note that you also can make one called logback.groovy to use instead or make a testing version called logback-test.xml.

You dont need to do anything with the files other than this, since JDA uses it automatically by itself.

You can learn more about logging in http://logback.qos.ch/manual/configuration.html#auto_configuration

And now, we should use `LoggerFactory` from slf4j to get the logger of the current class, I will do it in the very beginning kf the main method:
```java
LoggerFactory logger = LoggerFactory.getLogger("Main"); // because current file is Main
```
And now can replace the stdout in the main class (`System.out.println("Im ready, Im ready, Im ready!"`) with `logger.debug("Im ready, Im ready, Im ready!")`

Thats it for this one, see you!