# Embeds

Now we're gonna learn all about embeds! 

Here's how an embed looks like:
![embed](https://imgur.com/a/yOb5n)

Now, you see, embeds are very straight forward, to make them, we need to use the class `EmbedBuilder` like this:

```
EmbedBuilder eb = new EmbedBuilder();
```

Here, we made a new embedbuilder to build an embed, now let's see how to change the color, and add fields, and add a thumbnail, and add a picture, and add an author, etc.

```java
eb.setColor(0xFF00FF);
// this will change the color of the embed
``` 
In here, this is supposed to change the color of the embed, you need to enter a hex, and switch the # with 0x so if you neededd to make it white, instead of #FFFFFF you'd type 0xFFFFFF

```java
eb.setTitle("this is a title!");
// this will set the title of the embed to this is a title! 
``` 
This will set the title of the embed to the string inside the parentheses, if i wanted the title to say "Woo!" then I'd set the title of the embed to this: `eb.setTitle("Woo!")` its that easy!

```java
eb.setDescription("This is a description!");
// this will change the description of the embed to This is a description!
``` 
This will set the description to "This is a description!" And the description is under the title of the embed.

```java
eb.setAuthor("M.A", "iconurl");
// this sets the name of the author to M.A, and sets the icon of the author to a url!
```

This uses the Author attribute name to M.A, and the icon to a URL, be sure to include `https://` or `https://` in the URL or it wont work.

```java
eb.setThumbnail("url");
// sets the thumbnail to a picture from a url
```
This sets the thumbnail to a picture from the url, be sure to include `https://` or `http://` in the link or it wont work.

```java
eb.addField("name of the field", "value of the field", false); // or you can put true instead
// you can choose true or false in the inline field (the 3rd argument, could use true or false, I did false
```
This is an attribute that adds a field to your embed, it has 2 strings and an inline boolean.
Here's an example on inline and not inline fields:
![Diffirence between Inline and Not](https://i.imgur.com/gnjzCoo.png)

As you can see, the inline fields are all in one line,

![Inline fields](https://i.imgur.com/Ky0KlsT.png)

But the not inline field takes another line.

The 2 strings `name` and `value` both show up in the example shown up, the name being the bold, the upper one, the value being the little transparent one.

![Name and value](https://i.imgur.com/tB6tYWy.png)

Also, in jda, you can add empty fields!
to add empty fields in, you must do:

```java 
eb.addBlankField(true);
``` 
That'll make the field inline so its in the same line.

> NOTICE: You can only put 3 inline fields at one line.

![fields](https://i.imgur.com/lQqgH3H.png)

This shows a blank not inline field!
To do that, the code would be something like this

```java
eb.addBlankField(false);
```
Thats all about blank fields! now, moving on to:

Embed Images!

This is pretty easy, you just need to do:

```java
eb.setImage("url");
```

The URL must have the `https://` or `http://` with it or it wouldn't show. You can also use `attachment://path/to/file.png` to do it too.


Now, onto footer!

![Footers](https://i.imgur.com/jdf4sbi.png)

This is a footer, its made using this:

```java
eb.setFooter("this is a footer", "iconurl");
```
This is a how you set the Footer, again, with the iconURL starts with `https://` or `http://`

![Timersamps](https://i.imgur.com/YP4NiER.png)
Now, into timestamps!

Timestamps are made in JDA with:

```java
eb.setTimestamp(Instant.now());
``` 
This will make the timestamp take the exact time of when the embed was made and sets as the timestamp!

Now, this will set the attributes of the embed, but not put it to a message and share the message! to do that, we will code the following:

```java
MessageBuilder message = new MessageBuilder();

message.setEmbed(eb.build());
MessageChannel.sendMessage(message.build().queue());
```

> NOTICE: you can also chain these calls! for an example:

```java
EmbedBuilder eb = new EmbedBuilder()
     .setTitle("This is a title :blush:")
     .setDescription("Im kewl")
     .setColor(0xC0FFEE);
    
// then later on we can do this

MessageBuilder message = new MessageBuilder();

message.setEmbed(eb.build());

MessageChannel.sendMessage(message.build().queue());
```

Now, for real, thats all about embeds!

Finally, to [ReadyEvent and Logging!](./readyevent-and-logging)