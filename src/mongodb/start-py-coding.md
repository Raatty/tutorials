# Async Python MongoDB

So, welcome to this section where I go over an async Python driver for MongoDB - `motor`.
__Docs:__ https://motor.readthedocs.io/en/stable/

To get started just install this module preferably via pip: `pip install motor` (use accoordingly)

Now, to start with `asyncio` you got to get `AsyncIOMotorClient` which comes from `motor.motor_asyncio`. Then you need to provide it two arguments at the least:`host` and `port`. Optionally you may need to provide a `password` field in case you secured your server that way. (__Note:__ You can leave the host and port args out if you wanna use the default values but that's usually not what you want). Alternatively, you can use the full mongo
Here's an example! 
```py
from motor.motor_asyncio import AsyncIOMotorClient

# DO NOT KEEP THIS ON YOUR CODE
# PUT IT SOMEWHERE SAFE
mongo_credentials = {
    "host": "mongo",
    "port": 27017,
    "password": "foobar"
}

mongo_client = AsyncIOMotorClient(**mongo_credentials)
```
And there you go! You got yourself an async client for MongoDB!

However, as you know, MongoDB is organised into Databases, which contain Collections which contains Documents. Graphically speaking Mongo's "data tree" looks like this 
```
Mongo Server
  |
  ├-Collections
    |
    ├- Documents
```
Therefor we must get a database first. You have two ways of getting a database:

1. Use key-index notation → `main_db = mongo_client["main"]`
2. Use dot notation → `main_db = mongo_client.main`

To get a collection you use a similar process, but using a database as a starting point. Example: `my_collection = main_db.my_collection`.

Now, there are a lot of operations you can do on your data and for a MongoDB query syntax tutorial I suggest [**this**](https://www.tutorialspoint.com/mongodb/) guide on [Tutorial's Point](https://tutorialspoint.com). The [**MongoDB Docs**](https://docs.mongodb.com/) are also a really great place to start.

---

You are all set to go on using this MongoDB connection! I'll meet you at the [Main Python Methods](./main-py-methods.html) section where I go over some of the primary methods you need to know for Python.
